# Basic configuration

HISTFILE=$ZSHR/histfile
HISTSIZE=1000
SAVEHIST=1000
setopt appendhistory autocd 
[ -r /etc/profile.d/cnf.sh ] && . /etc/profile.d/cnf.sh

# End Basic conf
